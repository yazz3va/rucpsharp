﻿using RUCP.ClientSocket;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SurvivalServer.Profiles
{
    class CharacterInfo
    {
        public int id { get; set; }
        public int sessionkey { get; set; }
        public PlayerInfo player { get; set; }
        public ClientInfo client { get; set; }


        public CharacterInfo()
        {

        }
       public CharacterInfo(PlayerInfo pl, ClientInfo cl, int id)
        {
            player = pl;
            client = cl;
            this.id = id;
        }
    }
}
