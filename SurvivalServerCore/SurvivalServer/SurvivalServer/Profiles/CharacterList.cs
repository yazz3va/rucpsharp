﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SurvivalServer.Profiles
{
    class CharacterList
    {
        public static ConcurrentDictionary<int, CharacterInfo> characters = new ConcurrentDictionary<int, CharacterInfo>();
       // private static CharacterList<T> this_obj = new CharacterList<T>();

        public static bool Put(int id, CharacterInfo info)
        {
          return  characters.TryAdd(id, info);
        }

        public static bool GetCharecter(int id, out CharacterInfo info)
        {
           return characters.TryGetValue(id, out info);
        }

        public static void Remove(int id)
        {
            if (characters.ContainsKey(id))
            {
                CharacterInfo character_info;
                CharacterList.characters.TryRemove(id, out character_info);
                character_info = null;
            }
        }
      
       
    }
}
