﻿using RUCP.Sender;
using RUCP.Tools;
using SurvivalServer.HandlerType;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SurvivalServer.Profiles
{
    class Profile : HandlerPack
    {
        private CharacterInfo character_info = null;
        private RegisteredTypes type;

        public Profile()
        {
            character_info = new CharacterInfo();
            character_info.player = new PlayerInfo();
            type = RegisteredTypes.Instance();
        }



        void HandlerPack.ReadFromConnection(Packet pack)
        {
            type[pack.getType()](pack, character_info);
        }

        void HandlerPack.CloseConnection()
        {
            type[Types.selfCharacterRemove](null, character_info);
        }
    }
}
