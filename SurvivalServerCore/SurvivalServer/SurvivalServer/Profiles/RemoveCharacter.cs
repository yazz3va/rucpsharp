﻿using RUCP.ClientSocket;
using RUCP.Sender;
using RUCP.Server;
using RUCP.Tools;
using SurvivalServer.HandlerType;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SurvivalServer.Profiles
{
    class RemoveCharacter
    { 
        public static void SelfRemove(Packet pack, CharacterInfo info)
        {
         //   Console.WriteLine($"удаление персонажа: {info.id}  <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
            SaveCharacter(info, info.id, info.client);

            pack = new Packet(info.client, Channel.Reliable, 1);
            pack.writeType(Types.selfCharacterRemove);

            pack.write((byte)1);


            SenderClass.send(pack);

            //Отправка информации о вышедшем игроке всем игрокам
            InfoToAllCharacters(info);
            CharacterList.Remove(info.id);
        }

        private static void SaveCharacter(CharacterInfo info, long id, ClientInfo client)
        {
             //TODO: сохронение в базу игрока
        }

        //Отправка пакета с информацией о одном персонаже всем персонажем
        private static void InfoToAllCharacters(CharacterInfo info)
        {


         //   Console.WriteLine($"В массиве на отправку всем игрокам {CharacterList.characters.Count} ");
            foreach (KeyValuePair<int, CharacterInfo> entry in CharacterList.characters)
            {
                Packet pack = new Packet(entry.Value.client, Channel.Reliable, 4);
                pack.writeType(Types.CharacterRemove);
                pack.write(info.id);
                SenderClass.send(pack);
            }
        }
    }
}
