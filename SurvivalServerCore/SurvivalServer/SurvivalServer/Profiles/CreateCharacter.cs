﻿using RUCP.ClientSocket;
using RUCP.Sender;
using RUCP.Server;
using RUCP.Tools;
using SurvivalServer.HandlerType;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace SurvivalServer.Profiles
{
    class CreateCharacter
    {
        public static void SelfCreate(Packet pack, CharacterInfo info)
        {
            LoadingCharacter(info, pack.ReadInt(), pack.ReadInt(), pack.getClient());

            pack = new Packet(info.client, Channel.Reliable, 12);
            pack.writeType(Types.selfCharacterCreate);

            pack.write(info.id);
            pack.write(info.player.posX);
            pack.write(info.player.posY);

            SenderClass.send(pack);
           
            //Отправка информации о зашедшем игроке всем игрокам
            InfoToAllCharacters(info);
            //Отправка информации о всех игроках в игре зашедшему игроку
            AllInfoToCharater(pack.getClient());
            CharacterList.Put(info.id, info);
        }

        private static void LoadingCharacter(CharacterInfo info, int id, int key, ClientInfo client)
        {
            info.id = id;
            info.client = client;

            info.player.posX = 0.0f;
            info.player.posY = 0.0f;
        }

        //Отправка пакета с информацией о одном персонаже всем персонажем
        private static void InfoToAllCharacters(CharacterInfo info)
        {
    

          //  Console.WriteLine($"В массиве на отправку всем игрокам {CharacterList.characters.Count} ");
            foreach (KeyValuePair<int, CharacterInfo> entry in CharacterList.characters)
            {
                Packet pack = new Packet(entry.Value.client, Channel.Reliable, 12);
                pack.writeType(Types.CharacterCreate);
                pack.write(info.id);
                pack.write(info.player.posX);
                pack.write(info.player.posY);
                SenderClass.send(pack);
            }
        }
        private static void AllInfoToCharater(ClientInfo info)
        {
            
           // Console.WriteLine($"В массиве на отправку игроку игроков {CharacterList.characters.Count} ");
            foreach (KeyValuePair<int, CharacterInfo> entry in CharacterList.characters)
            {
                Packet pack = new Packet(info, Channel.Reliable, 12);
                pack.writeType(Types.CharacterCreate);
                pack.write(entry.Value.id);
                pack.write(entry.Value.player.posX);
                pack.write(entry.Value.player.posY);
                SenderClass.send(pack);
            }
        }
    }



}
