﻿using RUCP.Sender;
using RUCP.Server;
using RUCP.Tools;
using SurvivalServer.HandlerType;
using SurvivalServer.Profiles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SurvivalServer.Physics
{
    class PlayerMove
    {

        public static void Move(Packet pack, CharacterInfo info)
        {
            info.player.posX = BitConverter.ToSingle(pack.getData(), pack.ReadIndex(4));
            info.player.posY = BitConverter.ToSingle(pack.getData(), pack.ReadIndex(4));
            ToAllCharactes(info);
        }

        private static void ToAllCharactes(CharacterInfo info)
        {


            foreach(KeyValuePair<int, CharacterInfo> entry in CharacterList.characters)
            {
                if (entry.Value.id == info.id) continue;

                Packet pack = new Packet(entry.Value.client, Channel.Unreliable, 12);
                pack.writeType(Types.CharacterMove);
                pack.write(info.id);
                pack.write(info.player.posX);
                pack.write(info.player.posY);
              
                
                SenderClass.send(pack);
            }
        }
    }
}
