﻿using MySql.Data.MySqlClient;
using SurvivalServer.DataBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SurvivalServer.DataBaseQuery
{
    class LoginTable
    {
        public static int GetSessionKey(int id)
        {
            int key = 0;
            lock (DBHandler.Instance())
            {
                using (MySqlDataReader reader = DBHandler.ExecuteReader("SELECT sessionkey FROM login_table WHERE id=" + id + ";"))
                {
                    if (reader.Read())
                    {
                        key = reader.GetInt32(0);
                    }
                }
            }
            return key;
        }
    }
}
