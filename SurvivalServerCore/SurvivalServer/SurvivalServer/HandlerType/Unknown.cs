﻿using RUCP.Tools;
using SurvivalServer.Profiles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SurvivalServer.HandlerType
{
    class Unknown
    {

        public static void Type(Packet pack, CharacterInfo info)
        {
            Console.WriteLine($"Незарегестрированный тип '{pack.getType()}' " +
                $"от пользователя '{pack.getClient().getAdress().Address.ToString()}' : {pack.getClient().getAdress().Port}");
        }
    }
}
