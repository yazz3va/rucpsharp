﻿using RUCP.Tools;
using SurvivalServer.Physics;
using SurvivalServer.Profiles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SurvivalServer.HandlerType
{
    delegate void ChannelRead(Packet pack, CharacterInfo info);
   

     class RegisteredTypes
    {
      private static ChannelRead[] read = null;

        private static RegisteredTypes reg = new RegisteredTypes();

        public static RegisteredTypes Instance()
        {
            return reg;
        }

        public ChannelRead this[int index]
        {
            get
            {
                if(index < 0 || index >= read.Length) return read[0];
                if (read[index] != null) return read[index];
                return read[0];
            }
        }

        public static void RegisterTypes()
        {
            if (read != null) return;
            read = new ChannelRead[256];

            read[0] = Unknown.Type;
            read[Types.ConnectionGameServer] = CreateCharacter.SelfCreate;
            read[Types.PlayerMove] = PlayerMove.Move;
            read[Types.selfCharacterRemove] = RemoveCharacter.SelfRemove;
        }
    }
}
