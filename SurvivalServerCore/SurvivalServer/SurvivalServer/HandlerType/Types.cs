﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SurvivalServer.HandlerType
{
    public static class Types
    {
        public const short ConnectionGameServer = 1;// С->S отпровляет запрос серверу на создание профиля игрока 
        public const short selfCharacterCreate = 2; //s->c Информация для создания своего персонажа
        public const short CharacterCreate = 3; //S-C данные для создания обьекта персонажа на клиенте 
        public const short PlayerMove = 4; //C->S->C Отправка на сервер нового положение игрока и отпрака обратно подтвержденного положение
        public const short CharacterMove = 5; //S->C Отправка на клиент положения персонажей
        public const short CharacterRemove = 6; //S->C Удаляет обьект персонажа на клиенте
        public const short selfCharacterRemove = 7; // S->C Выход из карты
        public const short Login = 8;  //C->S отпровляет логин и пароль на сервер
        public const short Registration = 9; //C->S отпровляет логин и пароль  для регистрации акаунта
        public const short LoginInfo = 10; //S->C  Отпровляет код ошибки или успешного выполнение запроса
        public const short LoginOk = 11; //S->C Логин успешен
        public const short ServersList = 12; //C->S запрос на получение списка серверов S->C Список серверов
    }
}
