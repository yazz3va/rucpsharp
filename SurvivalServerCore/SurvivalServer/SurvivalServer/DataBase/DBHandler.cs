﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SurvivalServer.DataBase
{

    class DBHandler
    {
        private static string Connect = "Database=survivalLDB;" +
            "Data Source=127.0.0.1;" +
            "Port=3306;"+
            "User Id=yazZ3va;" +
            "Password=5475269qaz";
        //Переменная Connect - это строка подключения в которой:
        //БАЗА - Имя базы в MySQL
        //ХОСТ - Имя или IP-адрес сервера (если локально то можно и localhost)
        //ПОЛЬЗОВАТЕЛЬ - Имя пользователя MySQL
        //ПАРОЛЬ - говорит само за себя - пароль пользователя БД MySQL
        private static MySqlConnection myConnection = new MySqlConnection(Connect);
        private static DBHandler this_db = new DBHandler();

        private DBHandler()
        {

        }

        ~DBHandler()
        {
            if (myConnection != null)
                myConnection.Close();
            Console.WriteLine("Close DB");
        }

        public static DBHandler Instance()
        {
            return this_db;
        }

        public static MySqlDataReader ExecuteReader(string CommandText)
        {
            int cicle = 0;
            while (myConnection.State != ConnectionState.Open && cicle < 3) { cicle++; Connection(); }
            MySqlDataReader MyDataReader = null;
            try
            {
                MySqlCommand myCommand = new MySqlCommand(CommandText, myConnection);

                MyDataReader = myCommand.ExecuteReader();

            }
            catch (InvalidOperationException)
            {
                Console.WriteLine("Запрос не удалось обработать, соеденение с Бд отсуствует");
            }
            catch (MySqlException)
            {
                Console.WriteLine("Неверный синтаксис: " + CommandText);
            }
            return MyDataReader;
        }

        public static void ExecuteNonQuery(string CommandText)
        {
            int cicle = 0;
            while (myConnection.State != ConnectionState.Open && cicle < 3) { cicle++; Connection(); }

            try
            {
                MySqlCommand myCommand = new MySqlCommand(CommandText, myConnection);

                myCommand.ExecuteNonQuery();
            }
            catch (InvalidOperationException)
            {
                Console.WriteLine("Запрос не удалось обработать, соеденение с Бд отсуствует");
            }
            catch (MySqlException)
            {
                Console.WriteLine("Неверный синтаксис: " + CommandText);
            }

        }

        public static void Connection()
        {
            try
            {
               if(myConnection.State != ConnectionState.Open) myConnection.Open();
            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Не удалось соедениться с БД");
                Console.WriteLine(ex.ToString());
            }
            //if (myConnection.State == ConnectionState.Open) Console.WriteLine("Open DB");
            //  else Console.WriteLine("Dont open");
        }
    }
}
