﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using RUCP.Tools;
using RUCP.Server;
using System.Threading;

namespace RUCP.ClientSocket
{

    public class ClientInfo
    {





        private IPEndPoint adress = null;
        private HandlerPack handler_pack;
        private int ping = 500;
        private long id = 0;
        private bool online = true;
        private Packet[] buffer_receive;
        private Packet[] buffer_send;
        // private volatile int last_confirm = 0;
        private volatile ushort last_send = 0;
        //   private const ushort max_send = 65000;
        public int CheckingTime { get; set; }

        // private DatagramPacket main_packet;
        // private DatagramPacket main_packet;

        public ClientInfo(IPEndPoint a,  HandlerPack h, long id)
        {
            buffer_receive = new Packet[100];
            buffer_send = new Packet[100];
            adress = a;
            handler_pack = h;
            CheckingTime = Time.deltaTime;
            this.id = id;
        }

        public bool Online
        {
            get { return online; }
        }

        public int getPing()
        {
            return ping;
        }

        public void SetPing(int ping)
        {
            this.ping += ping;
            this.ping /= 2;
        }

        public IPEndPoint getAdress()
        {
            return adress;
        }

        public bool OpenConnection(Packet pack)
        {
            if (handler_pack != null) return handler_pack.OpenConnection(pack);
            Console.WriteLine("error");
            return false;
        }


        /***
         * Передает пакеты в Обрабатывающий класс
         * @param pack
         */
        public void handlerPack(Packet pack)
        {
            if (handler_pack != null) handler_pack.ReadFromConnection(pack);
            else Console.WriteLine("error");
        }


        /// <summary>
        /// Удаление ClientInfo из списка соедений и вызов завершающего метода в профиле
        /// </summary>
        public void CloseConnection()
        {
            if (online)
            {
                if (handler_pack != null) handler_pack.CloseConnection();
                ClientList.RemoveClient(id);
                online = false;
            }

        }

        /// <summary>
        /// Отсылает клиенту команду на отключения
        /// </summary>
        public void Disconnect()
        {
            byte[] rec_data = new byte[3];
            rec_data[0] = (byte)Channel.Disconnect;
            ServerMain.getSocket().Send(rec_data, rec_data.Length, adress);
        }


        public void SetAsk(ushort number)
        {
            int buffer_pos = number % buffer_send.Length;
            if (buffer_send[buffer_pos] != null)
            {
                buffer_send[buffer_pos].setAck(true);
                buffer_send[buffer_pos] = null;
            }
        }

        public void InsertBuffer(Packet pack)
        {
            lock (buffer_send)
            {
                pack.setNumber(last_send);
                buffer_send[last_send % buffer_send.Length] = pack;

                last_send++;
            }
        }

        /***
         * Отправляет АСК пришедшего пакета
         * Если пакет пришол повторно false
         * @return
         */
        public bool CheckReliablePack(Packet pack)
        {

            try
            {


                byte[] rec_data = new byte[3];
                rec_data[0] = Channel.ACK; // ACK
                rec_data[1] = pack.getData()[1];
                rec_data[2] = pack.getData()[2];
                ServerMain.getSocket().Send(rec_data, rec_data.Length, adress);

                int index = pack.getNumber() % buffer_receive.Length;

                lock (buffer_receive)
                {
                    if (buffer_receive[index] == null || !buffer_receive[index].Equals(pack))
                    {
                        buffer_receive[index] = pack;
                        return true;
                    }
                }
                return false;
            }
            catch (Exception e)
            {
                // TODO Auto-generated catch block
                Console.WriteLine(e.ToString());
                return false;
            }
        }



    }

}
