﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RUCP.Tools;

namespace RUCP.Sender
{

    public class SendBuffer
    {

       
        /***
         * Первый пакет (с ближайшим временем переоотправки) в цепочке пакетов сортированных по времени переотправки
         */
        private GhostPacket<Packet> linked = null;
        /***
         * Последний пакет (с наибольшим временем переоотправки) в цепочке пакетов сортированных по времени переотправки
         */
     //   private GhostPacket end_ghost = null;

      //  private const int size_buffer = 10000;//Размер буфера <<<<
                                             /***
                                             * Массив из цепочки Pack
                                             */


        private static SendBuffer this_buffer = new SendBuffer();


        public SendBuffer()
        {
          
         //   buffer = new Packet[size_buffer];
            linked = new GhostPacket<Packet>();
        }

        public static SendBuffer Instant()
        {

                return this_buffer;
        }

     /*   public void ack(int number)
        {
            number %= buffer.Length;
          //  Console.WriteLine("Number ack: " + number);
            if (buffer[number] != null) { buffer[number].setAck(true); buffer[number].SetPing(); }
            else Console.WriteLine($" >>> ne OK {number} <<< ");
        }*/
        /***
         * Добовлят пакет в очередь ожидание неподтвержденных пакетов и сортирует его по времени переотправки
         * @param pack
         */
        public void put(Packet pack)
        {
            
          //  int number = pack.getNumber() % buffer.Length;

            //  if (buffer[number] == null)
         //   buffer[number] = pack;
          //  else Console.WriteLine(" >>> ERROR...Buffer: The buffer is full ...ERROR <<< ");

            lock (linked)
            {
                linked.Insert(pack);
            }
        }

        /***
         * Возвращает и удаляет первый элемент в цепочке,также удаляет из массива пакетов АСК, если такого элемента нет возврощает null
         * @return
         */
        public Packet get()
        {
 
            lock (linked)
            {
                if (linked.Count > 0)
                {

               //     buffer[pack.getNumber() % buffer.Length] = null;
                    return linked.Get();
                }
                else return null;
            }
        }


        /***
         * Возвращает и удаляет первый элемент в цепочке,но не удаляет из массива пакетов АСК, если такого элемента нет возврощает null
         * @return
         */
  /*      public Packet getResend()
        {
            lock (linked) {
                if (linked.Count > 0)
                {
                    return linked.Get();
                }
                else return null;
            }
            
        }*/

        public Packet GetFirstPack()
        {

                return linked.Pack;

        }

        public bool getAck()
        {
            if (linked.Count > 0) return linked.Pack.isAck();
            else return false;
        }

        /***
         * Возврощает время отправки первого элемента в цепочке, если такого элемента нет возврощает 0
         * @return
         */
        public int getTime()
        {
            if (linked.Count > 0) return linked.Pack.getTime_next_send();
            else return 0;
        }

        /***
         * Возврощает true - если в буфере есть элементы
         * false - пустой буфер
         * @return
         */
        public bool isGet()
        {
            return (linked.Count > 0);
        }
    }

}
