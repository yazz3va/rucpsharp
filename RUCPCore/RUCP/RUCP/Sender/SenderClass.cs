﻿using RUCP.Server;
using RUCP.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RUCP.Sender
{

    public class SenderClass
    {

        private static SendBuffer buffer = SendBuffer.Instant();
        private static Resender resender = Resender.Instance();
        private static int number_send = 0;

        



        /***
         * Отправка пакетов
         * @param pack
         */
        public static void send(Packet pack)
        {
            try
            {
                if (pack.getChannel() == Channel.Reliable)
                {
                    pack.getClient().InsertBuffer(pack);

                    buffer.put(pack); //Запись на переотправку

                    pack.SetTimeSend();

                   
                }
              
                ServerMain.getSocket().Send(pack.getData(), pack.getData().Length,
                        pack.getClient().getAdress());
            }
            catch (Exception e)
            {
                // TODO: Обработка исключение в Sender
                Console.WriteLine(e.ToString());
            }
        }
    }
}
