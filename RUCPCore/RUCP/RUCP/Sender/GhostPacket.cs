﻿using RUCP.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RUCP.Sender
{

    /**
     * Created by yazZ3 on 30.04.2017.
     */
    public class GhostPacket<T> where T:IComparable<T>
    {
        private GhostPacket<T> next = null;
        private T pack = default(T);
        private GhostPacket<T> prev = null;

        private int size = 0;

        public int Count
        {
            get
            {
                return size;
            }
        }

        public T Pack
        {
            get
            {
                return next.pack;
            }
        }

        public GhostPacket()
        {
            this.next = this;
            this.prev = this;
        }
        private GhostPacket(T currentPack, GhostPacket<T> next, GhostPacket<T> prev)
        {
           this.pack = currentPack;
            this.next = next;
            this.prev = prev;
        }
        public void Insert(T obj)
        {
            GhostPacket<T> element = prev;
 
            while (obj.CompareTo(element.pack) > 0) 
                element = element.prev;
            
            GhostPacket<T> newEntry = new GhostPacket<T>(obj, element.next, element);
            newEntry.prev.next = newEntry;
            newEntry.next.prev = newEntry;

            Interlocked.Increment(ref size);
        }

        public T Get()
        {
            if (size == 0) return default(T);
            T t = next.pack;

            GhostPacket<T> element = next;

            next = element.next;
            next.prev = this;

            element.next = null;
            element.prev = null;
            element = null;
            Interlocked.Decrement(ref size);
            return t;
        }
    }

}
