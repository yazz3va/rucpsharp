﻿
using RUCP.Tools;
using System;
using System.Net.Sockets;
using System.Threading;
using System.Net;
using RUCP.ClientSocket;
using RUCP.Sender;
using System.Collections.Concurrent;

namespace RUCP.Server
{
   public class ServerMain

    {

        /// <summary>
        /// Сокет для отправки получение датаграмм
        /// </summary>
        private static UdpClient datagram_socket;
     
        /***
         * Буфер для хранение пакетов, если все потоки заняты
         */
        private static ConcurrentQueue<Packet> buffer;
        /***
         *  Пакет с данными
         */
        private Packet pack;
        private static Handler handler;
        private int port;
        private IPEndPoint RemoteIpEndPoint = null;

        public void setHandler(Handler han)
        {
            handler = han;
        }

        public ServerMain(int port)
        {

            this.port = port;
        }

        public void start()
        {
            try
            {
                buffer = new ConcurrentQueue<Packet>();
                //Создание сокета по порту порт и датаграммы для считывание данных
                datagram_socket = new UdpClient(port);

                int SIO_UDP_CONNRESET = -1744830452; //Отключает вызов исключения о недоступности конечного хоста
                datagram_socket.Client.IOControl(
            (IOControlCode)SIO_UDP_CONNRESET,
            new byte[] { 0, 0, 0, 0 },
            null
        );

                CheckingConnections.Instance();
                //Запуск потока считывание датаграмм
                /*  Thread th = new Thread(run);
                  //th.setDaemon(true);
                  th.Start();*/
                run();


            }
            catch (SocketException e)
            {
                Console.WriteLine(e.ToString());
                // TODO Auto-generated catch block

            }
        }

        public static UdpClient getSocket()
        {
            return datagram_socket;
        }

        public static ConcurrentQueue<Packet> Buffer
        {
            get
            {
                return buffer;
            }
        }
        public static HandlerPack GetHandlder()
        {
            return handler.getHandler();
        }

        //Считывание датаграм из сокета
     
    public void run()
        {
            //byte[] data_pack;

           Console.WriteLine("RUCP ver. 0.4 alpha ");
            //Создание потоков оброботчиков(количество ядер х2)
            HandlerThread[] pool_handler = new HandlerThread[Environment.ProcessorCount * 2];
            //Запускаем потоки
            for (int i = 0; i < pool_handler.Length; i++)
            {
                pool_handler[i] = new HandlerThread();
                pool_handler[i].Start();
            }


            try
            {
                bool loop = true;
                while (true)
                {
                    try
                    {
                        //Считывание датаграм
                        // Ожидание дейтаграммы
                        byte[] receiveBytes = datagram_socket.Receive(
                           ref RemoteIpEndPoint);


                        //Создание нового пакета для хранение данных
                        pack = new Packet(receiveBytes, RemoteIpEndPoint);

                        loop = true;

                        //Передаем данные на оброботку первому свободному потоку
                         //Опрашиваем потоки по очереди проверяя нет ли свободного потока
                        foreach (HandlerThread handlerThread in pool_handler)
                        {
                            if (!handlerThread.isWork())
                            { //Если поток свободен
                                handlerThread.pushTask(pack); //Передаем данные
                                loop = false; //Прерываем цикл опроса потоков
                                break;
                            }
                        }
                        if (loop) buffer.Enqueue(pack); //Если пакет не был передан потоку, помещаем его в буфер



                    } catch(SocketException s)
                    {
                        Console.WriteLine("exception ServerMain ");
                        Console.WriteLine(s.TargetSite);
                    }
                }
            }
            catch (Exception e)
            {
                // TODO Auto-generated catch block
                Console.WriteLine(e.ToString());
            }
        }

    }
}
