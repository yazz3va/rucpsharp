﻿using RUCP.ClientSocket;
using RUCP.Sender;
using RUCP.Tools;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RUCP.Server
{
    class CheckingConnections
    {
        private const int time_check = 5 * 60000;
        private ConcurrentQueue<ClientInfo> list_checking = new ConcurrentQueue<ClientInfo>();

        private static CheckingConnections this_class = new CheckingConnections();

        public static CheckingConnections Instance()
        {
            return this_class;
        }

        CheckingConnections()
        {
            Initial();
        }

        public void InsertClient(ClientInfo client)
        {
            list_checking.Enqueue(client);
        }

        public void Initial()
        {
            Thread th = new Thread(Checking);
            th.Start();
        }

        private void Checking()
        {
            ClientInfo client = null;
            int sleep;
 
            while (true)
            {
   
                if(client != null && client.Online)
                {
                    SendCheckPack(client);//Отправка пакета для проверки соеденения
                    list_checking.Enqueue(client);//Вставка клиента в конец очереди для повторной проверки
                    client.CheckingTime = Time.deltaTime;
                    client = null;

                }
                sleep = time_check;
                while(list_checking.TryDequeue(out client))
                {
                    if (!client.Online) { client = null; continue; }

                    sleep = (client.CheckingTime + time_check) - Time.deltaTime;
                    break;
                }

                if (sleep > 0) Thread.Sleep(sleep);
            }
        }

        private void SendCheckPack(ClientInfo client)
        {
            Packet pack = new Packet(client, Channel.Reliable, 0);
            pack.writeType(0);
            SenderClass.send(pack);
        }

    }
}
