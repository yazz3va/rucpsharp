﻿using RUCP.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using RUCP.Sender;
using RUCP.ClientSocket;
using System.Collections.Concurrent;

namespace RUCP.Server
{

    public class HandlerThread
    {
        /***
         * Метка занят ли поток оброботкой какойто задачи
         * true = занят 
         * false = свободен
         */
        private bool work_trhead = false;

        /***
         * Очередь данных(пакетов) на обработку
         */
        private ConcurrentQueue<Packet> array;

        /***
         * Поток оброботки задач этого класса
         */
        private Thread current_thread;

        /**
         * Ид клиента задачу(пакет) которого обробатывет текущий поток 
         * 0 - нет клиента
         */
        //private long id_task_client = 0;
      //  private long id_client = 0;
     //   private int id_thread;
        //  private EventWaitHandle wh = new AutoResetEvent(true);


        //Конструктор
        public HandlerThread()
        {

            array = new ConcurrentQueue<Packet>();
          //  id_thread = i;
        }

        public void Start()
        {
            current_thread = new Thread(run);
            current_thread.Start();
        }
        /*public void setWorkTh(boolean w){
            work_trhead = w;
        }*/


        /***
         * Возврощает ид клиента задачу(пакет) которого обробатывет текущий поток 
         * @return
         */
      /*  public long getIdtask()
        {
            return id_client;

        }*/


        /***
         * Если поток занят возвращает true, false - свободен
         * @return
         */
        public bool isWork()
        {
            return work_trhead;
        }


        /*
         * Добовляет задачу в очередь задач
         */
        public void pushTask(Packet pack_in)
        {
            //Устанавливаем метку work_trhead = true? поток занят
            work_trhead = true;
            //Добовляем задачу в очередь
            array.Enqueue(pack_in);
         //   id_client = pack_in.getIdClient(); //Устанавливаем ИД текущего клиента
                                               //System.out.println("Add: "+array.add(pack)+" size: "+array.size()+" idth: "+id_thread);



            current_thread.Interrupt(); //Будим поток если он спит

        }


        public void run()
        {

            Packet packet = null;

            ClientInfo client = null;

            SendBuffer send_buffer = SendBuffer.Instant();

            while (true)
            {
                try
                {
                    while (array.TryDequeue(out packet) || ServerMain.Buffer.TryDequeue(out packet))
                    { //Получаем задачу(пакет) из очереди
                      //Если очередь не была пустой. Обрабатываем пакет

                        client = ClientList.getClient(packet.getIdClient());  //Получаем оброботчик(Сокет) конкретного клиента по ид


                        if (client != null)
                        {
                            packet.setClient(client);//Задаем ссылку на клиента для обратной отправки пакетов

                            if (packet.getChannel() == Channel.ACK)
                            {
                                client.SetAsk(BitConverter.ToUInt16(packet.getData(), 1));
                                continue;
                            }
                            else if (packet.getChannel() == Channel.Reliable)
                            {//Если пакет передан по надежному каналу
                                if (!client.CheckReliablePack(packet))//Отпровляет АСК подтверждение получение пакета
                                {
                                    continue; // Если пакет уже был получен ранее
                                }
                            }
                            else if (packet.getChannel() == Channel.Disconnect)
                            {
                                client.CloseConnection();
                                continue;
                            }
                            else if (packet.getChannel() == Channel.Connection)
                            {
                                ServerMain.getSocket().Send(packet.getData(), packet.getData().Length, packet.Adress);
                                continue;
                            }

                            if (packet != null)
                            { //Обработка пакета

                                client.handlerPack(packet);

                            }
                        }
                        else
                        {
                            //Если клиент хочет подключится
                            if (packet.getChannel() == Channel.Connection)
                            {
                                if (!ClientList.containsKey(packet.getIdClient()))
                                { //Если такого оброботчика(сокета) нет создаем новый
                                    ClientInfo _client = new ClientInfo(packet.Adress, ServerMain.GetHandlder(), packet.getIdClient());
                                    ClientList.putClient(packet.getIdClient(), _client);
                                    CheckingConnections.Instance().InsertClient(_client);//Вставка клиента в очередь проверки соеденение 
                                }
                                else Console.WriteLine("Такой клиент уже есть в списке");

                                ServerMain.getSocket().Send(packet.getData(), packet.getData().Length, packet.Adress); //отпровляем подтверждение клиенту
                                Console.WriteLine("online: " + ClientList.online());
                                continue;
                            }
                            else //Если клиент не из списка подключенных продолжает слать пакеты(Не дашла команда на отключение от сервера) послать повторно
                            {
                                byte[] rec_data = new byte[3];
                                rec_data[0] = (byte)Channel.Disconnect;
                                ServerMain.getSocket().Send(rec_data, rec_data.Length, packet.Adress);
                            }
                        }
                    }
                    work_trhead = false;
                    //Завершение оброботки задачи 

                    //Переводим поток в режим ожидания 
                    if (!work_trhead) current_thread.Join();//Усыпляем поток покуда главный поток не разбудит его


                    //Поток разбужен главным потоком, передана задача на оброботку
                }
                catch (Exception)
                {

                    //   Console.WriteLine("wake up thread: " + id_thread);
                }
                }

            }
        }

    }
