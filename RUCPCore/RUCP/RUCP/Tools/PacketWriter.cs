﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RUCP.Tools
{
    public partial class Packet : IComparable<Packet>
    {


        public bool write(long int_d)
        {
            write(BitConverter.GetBytes(int_d));
            return true;
        }
        /***
         * Записывает int  в заранее определнный массив данных на отправку.
         * Если размер данных для записи превышает размер массива возврощает exception
         * @param int_d
         * @return
         */
        public bool write(int int_d)
        {
            byte[] int_b = BitConverter.GetBytes(int_d);
            //    if((curren_pos+int_b.length) > pack.length) return false;
            data[curren_pos++] = int_b[0];
            data[curren_pos++] = int_b[1];
            data[curren_pos++] = int_b[2];
            data[curren_pos++] = int_b[3];
            return true;
        }

        /***
         * Записывает short  в заранее определнный массив данных на отправку.
         * Если размер данных для записи превышает размер массива возврощает exception
         * @param short_d
         * @return
         */
        public bool write(short short_d)
        {
            byte[] short_b = BitConverter.GetBytes(short_d);
            //   if((curren_pos+short_b.length) > pack.length) return false;
            data[curren_pos++] = short_b[0];
            data[curren_pos++] = short_b[1];
            data[curren_pos++] = short_b[2];
            data[curren_pos++] = short_b[3];
            return true;
        }

        /***
         * Записывает byte  в заранее определнный массив данных на отправку.
         * Если размер данных для записи превышает размер массива возврощает exception
         * @param byte_d
         * @return
         */
        public bool write(byte byte_d)
        {
            //  if((curren_pos+1) > pack.length) return false;
            data[curren_pos++] = byte_d;
            return true;
        }

        /***
         * Записывает byte[]  в заранее определнный массив данных на отправку.
         * Если размер данных для записи превышает размер массива возврощает exception
         * @param byte_d
         * @return
         */
        public bool write(byte[] byte_d)
        {

            //   if((curren_pos+byte_d.length) > pack.length) return false;
            Array.Copy(byte_d, 0, data, curren_pos, byte_d.Length);
            curren_pos += byte_d.Length;
            return true;
        }

        public bool write(float f)
        {
            return write(BitConverter.GetBytes(f));
        }


    }
}
