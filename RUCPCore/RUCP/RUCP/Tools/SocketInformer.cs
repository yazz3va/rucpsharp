﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace RUCP.Tools
{


    class SocketInformer
    {

        public static long getID(byte[] ip, int port)
        {
          //  Console.WriteLine($"recieve from {ip[0]}.{ip[1]}.{ip[2]}.{ip[3]} , port {port} ;");
            long id = 0;

            //IP
            id |= ((long)ip[0] << 56);
            id |= ((long)ip[1] << 48);
            id |= ((long)ip[2] << 40);
            id |= ((long)ip[3] << 32);

            //Port
            id |= (long)port;

            return id;
        }

        public static long getID(IPEndPoint RemoteIpEndPoint)
        {
            return getID(RemoteIpEndPoint.Address.GetAddressBytes(), RemoteIpEndPoint.Port);
        }

    }
}
