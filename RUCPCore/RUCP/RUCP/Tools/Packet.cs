﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RUCP.ClientSocket;
using System.Net;
using RUCP.Server;

namespace RUCP.Tools
{
    public partial class Packet:IComparable<Packet>
    {
        private const int ReliableHead = 5;
        private const int UnreliableHead = 3;

        private byte try_cicle = 0;
        /***
         * Данные для отправки
         */
        private byte[] data;
        /***
         * ИД клиента
         */
        private IPEndPoint adress;
        private ClientInfo client;
        /***
         * Время отправки пакета
         */
        private int time_send = 0;
        private bool ack = false;
        /***
         * текущая позиция записи, считыванние данных в массив
         */
        private int curren_pos = 0;

        /// <summary>
        /// Количество попыток переотправки пакета
        /// </summary>
        public byte sendCicle
        {
            get
            {
                return try_cicle;
            }
        }

        public IPEndPoint Adress
        {
            get
            {
                return adress;
            }
        }
        /***
         * Возврощает тип пакета
         * @return
         */
        public short getType()
        {
            if (data[0] == (byte)Channel.Reliable)
            {
                return BitConverter.ToInt16(data, 3);

            }
            else if (data[0] == (byte)Channel.Unreliable)
            {
                return BitConverter.ToInt16(data, 1);
            }
            return 0;
        }

        public bool isAck()
        {
            return ack;
        }

        /***
         * Задает метку доставки пакета.
         * Задается автоматически при получении пакета от клиента с подтверждением доставки
         * @param ack
         */
        public void setAck(bool ack)
        {
            this.ack = ack;
        }

        /***
         * Возврощает время повторной отправки пакета при неудачной попытке доставки
         * @return
         */
        public int getTime_next_send()
        {
            return time_send + client.getPing();
        }

        /***
         * Задает время повторной отправки пакета, при неудачной попытке доставки.
         * Задается автоматически при отправке!.
         */
        public void SetTimeSend()
        {
            time_send = Time.deltaTime;
            try_cicle++;
        }

        public long getIdClient()
        {
            return SocketInformer.getID(adress);
        }

        public void setClient(ClientInfo client)
        {
            this.client = client;
        }

        /***
         * Возвращает Класс с информацией о подключении.
         * Ип, порт, ид подключение
         * @return
         */
        public ClientInfo getClient()
        {
            return client;
        }

        private void SetCurrentPosIndex()
        {
            if (data[0] == Channel.Reliable)
            {
                curren_pos = ReliableHead;
            }
            else
            {
                curren_pos = UnreliableHead;
            }
        }

        /***
         * Позволяет использовать принятый пакет для обратной отправки отправителю.
         * Можно использовать только один раз
         * @param ch
         * @param len
         */
        public void reuse(byte channel, int len)
        {


            if (channel == (byte)Channel.Reliable)
            {
                data = new byte[len + ReliableHead];
                curren_pos = ReliableHead;
            }
            else
            {
                data = new byte[len + UnreliableHead];
                curren_pos = UnreliableHead;
            }

            data[0] = channel;
        }
        public void reuse()
        {
            SetCurrentPosIndex();
        }
        public Packet(ClientInfo client, byte channel, int len)
        {

            this.client = client;

            if (channel == (byte)Channel.Reliable)
            {
                data = new byte[len + ReliableHead];
                curren_pos = ReliableHead;
            }
            else
            {
                data = new byte[len + UnreliableHead];
                curren_pos = UnreliableHead;
            }

            data[0] = channel;
        }
        public Packet(byte[] pack, IPEndPoint RemoteIpEndPoint)
        {

            //Получаем ид клиента по адрессу и порту от которого пришла датаграмма
            adress = RemoteIpEndPoint;

            this.data = pack;


            SetCurrentPosIndex();
        }

       

        /***
         * Возврощает начальный индекс после заголовка, с которого начинаются данные в пакете
         * @return
         */
        public int getStartIndex()
        {
            if (data[0] == Channel.Reliable) return ReliableHead;
            else return UnreliableHead;
        }

        /***
         * Возврощает канал по которому будет\был передан пакет
         * @return
         */
        public byte getChannel()
        {
            return data[0];
        }


        /***
         * Задает порядковый номер отпровляемого пакета.
         *
         * @param n
         */
        public void setNumber(ushort number)
        {
          //  number = n;
            if (data[0] == Channel.Reliable)
            { 
                byte[] number_b = BitConverter.GetBytes(number);
                data[1] = number_b[0];
                data[2] = number_b[1];
            }
        }

        /***
         * Возврощает порядковый номер отпровленного пакета по надежному каналу
         * @return
         */
        public ushort getNumber()
        {
            return BitConverter.ToUInt16(data, 1);
        }


        /***
         * Возврощает массив с данным для Отправки
         * @return
         */
        public byte[] getData()
        {
            return data;
        }

        public void SetPing()
        {
            client.SetPing(Time.deltaTime - time_send);
        }
      

        /*
        Запись данных на передачу в массив
         */

        /***
         * Записывает тип пакета в заголовок
         * @param typ
         */
        public void writeType(short typ)
        {
            byte[] type_b = BitConverter.GetBytes(typ);
            if (data[0] == Channel.Reliable)
            {
                data[3] = type_b[0];
                data[4] = type_b[1];
            }
            else if (data[0] == Channel.Unreliable)
            {
                data[1] = type_b[0];
                data[2] = type_b[1];
            }
        }


        int IComparable<Packet>.CompareTo(Packet other)
        {
            if (other == null) return -1;
            if (other.getTime_next_send() < getTime_next_send()) return -1;
            return 1;
        }

        public override bool Equals(object obj)
        {
            Packet _pack = obj as Packet;
            if (_pack == null) return false;
            return data[1] == _pack.data[1] && data[2] == _pack.data[2];
        }
    }
}
