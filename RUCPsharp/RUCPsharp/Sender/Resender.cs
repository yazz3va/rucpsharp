﻿using RUCPsharp.Server;
using RUCPsharp.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RUCPsharp.Sender
{

    /**
     * Created by yazZ3 on 30.04.2017.
     */
    class Resender
    {

    private SendBuffer buffer;
    private int sleep_time = 0;
        private Thread this_thread;

        private static Resender this_resender = new Resender();
    public Resender()
    {
            this.buffer = SendBuffer.Instant();
            this_thread = new Thread(run);
            this_thread.Start();
    }
        public static Resender Instance()
        {
            lock (this_resender)
            {
               
                return this_resender;
            }
        }


    public void run()
    {
         //   Console.WriteLine("Ресендер запущен");
            while (true)
        {
                //Если в буффере есть пакеты
                if (buffer.isGet())
                {
                    //Если первый пакеет в очереди подтвержден удаляем его из очереди и переходим к следуюещему
                    if (buffer.getAck()) { buffer.get(); continue; }

                    //Если количество попыток переотправки пакета превышает 10, отключаем клиента
                    if (buffer.GetFirstPack().sendCicle > 10)
                    {
                        buffer.GetFirstPack().getClient().Disconnect();
                        buffer.get().getClient().CloseConnection();
                            continue;
                    }


                    sleep_time = buffer.getTime() - Time.deltaTime;



                    //Если время ожидание вышло переотпровляем пакет и ставим его в конец очереди
                    if (sleep_time <= 0)
                    {
                        Packet pack = buffer.get();

                        try
                        {
                            pack.SetTimeSend();
                            ServerMain.getSocket().Send(pack.getData(), pack.getData().Length,
                                    pack.getClient().getAdress());
                            buffer.put(pack);//Вставка в очередь неподтвержденных пакетов
                            continue;
                        }
                        catch (SocketException e)
                        {
                            Console.WriteLine("Ошибка отправки : " + e.ToString());
                        }
                        catch (Exception e)
                        {
                            //TODO: 
                            Console.WriteLine(e.ToString());
                        }
                    } //<< sleep <= 0
                  //Если время переотправки не пришло усыплям поток на заданное время
            }
            else
            {//Если нет пакетов в очереди усыпляем поток на 0.05с
                sleep_time = 50;
                //    Console.WriteLine("Буффер пустой");
            }
            try
            {
                  //  Console.WriteLine($"Thread sleep on : " + sleep_time);
                Thread.Sleep(sleep_time);
            }
            catch (Exception e)
            {
                    Console.WriteLine("exeption resender");
                    Console.WriteLine(e.ToString());
            }
        }
    }
        ~Resender()
        {
            Console.WriteLine("Уничтожение Resender");
        }
}

}
