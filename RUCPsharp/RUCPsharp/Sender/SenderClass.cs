﻿using RUCPsharp.Server;
using RUCPsharp.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RUCPsharp.Sender
{

    public class SenderClass
    {

        private static SendBuffer buffer = SendBuffer.Instant();
        private static Resender resender = Resender.Instance();
        private static int number_send = 0;

        



        /***
         * Отправка пакетов
         * @param pack
         */
        public static void send(Packet pack)
        {
            try
            {
                if (pack.getChannel() == Channel.Reliable)
                {
                    if(pack.getClient() == null) { Console.WriteLine("client == null"); return; }
                    pack.getClient().InsertBuffer(pack);

                    buffer.put(pack); //Запись на переотправку

                    pack.SetTimeSend();

                   
                }
              
                ServerMain.getSocket().Send(pack.getData(), pack.getData().Length,
                        pack.getClient().getAdress());
            }
            catch (NullReferenceException)
            {
                Console.WriteLine($"nullexc Type: {pack.GetTypePacket()}  Channel: {pack.getChannel()} Len: {pack.getData().Length}");
            }
            catch (Exception)
            {
                Console.WriteLine($" Type: {pack.GetTypePacket()}  Channel: {pack.getChannel()} Len: {pack.getData().Length}");
                // TODO: Обработка исключение в Sender
             //   Console.WriteLine(e.ToString());
            }
        }
    }
}
