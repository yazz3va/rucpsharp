﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RUCPsharp.ClientSocket
{

     class ClientList
    {
        /***
         * Список всех подключенных сокетов(Клиентов)
         */
        private static ConcurrentDictionary<long, ClientInfo> list_client = new ConcurrentDictionary<long, ClientInfo>();


        /***
         * добовляет в список клиента, если такой клиент уже ест ьв списке возврощает false
         * @param key
         * @param cl
         * @return
         */
        public static bool putClient(long key, ClientInfo cl)
        {
          return list_client.TryAdd(key, cl); 
        }

        /***
         * Проверяет есть ли клиент с заданным ид в списке клиетов
         * @param key
         * @return
         */
        public static bool containsKey(long key)
        {
            return list_client.ContainsKey(key);
        }

        /***
         * Возврощает клиента по ключу, если такого клиента нет возврощает null
         * @param key
         * @return
         */
        public static ClientInfo getClient(long key)
        {

                ClientInfo cl = null;
                 list_client.TryGetValue(key, out cl);
                return cl;
        }

        public static void RemoveClient(long id)
        {
            ClientInfo cl;
            list_client.TryRemove(id, out cl);
        }

        public static int online()
        {
            return list_client.Count;
        }
    }
}
