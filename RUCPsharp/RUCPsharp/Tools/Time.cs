﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RUCPsharp.Tools
{
    class Time
    {

        private static DateTime time_start = DateTime.Now;
        public static int deltaTime
        {
            get
            {
               
                return (int)((DateTime.Now.Ticks - time_start.Ticks) / TimeSpan.TicksPerMillisecond);
            }
        }
    }
}
