﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RUCPsharp.Tools
{
    /**
 * Created by yazZ3va on 14.04.2017.
 */
    public interface HandlerPack
    {
        bool OpenConnection(Packet pack);
        void ReadFromConnection(Packet pack);
        void CloseConnection();
    }
}
