﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RUCPsharp.Tools
{
    public partial class Packet : IComparable<Packet>
    {


        public void Write(long int_d)
        {
            Write(BitConverter.GetBytes(int_d));
        }
        /***
         * Записывает int  в заранее определнный массив данных на отправку.
         * Если размер данных для записи превышает размер массива возврощает exception
         * @param int_d
         * @return
         */
        public void Write(int int_d)
        {
            Write(BitConverter.GetBytes(int_d));
        }

        /***
         * Записывает short  в заранее определнный массив данных на отправку.
         * Если размер данных для записи превышает размер массива возврощает exception
         * @param short_d
         * @return
         */
        public void Write(short short_d)
        {
            Write(BitConverter.GetBytes(short_d));
        }

        /***
         * Записывает byte  в заранее определнный массив данных на отправку.
         * Если размер данных для записи превышает размер массива возврощает exception
         * @param byte_d
         * @return
         */
        public void Write(byte byte_d)
        {
            try
            {
                data[curren_pos++] = byte_d;
            }
            catch (IndexOutOfRangeException)
            {
                Console.Error.WriteLine($"индекс находится за границами массива id client: {GetIdClient()} type: {GetTypePacket()}");
            }
        }

        /***
         * Записывает byte[]  в заранее определнный массив данных на отправку.
         * Если размер данных для записи превышает размер массива возврощает exception
         * @param byte_d
         * @return
         */
        public void Write(byte[] byte_d)
        {
            try
            {
                Array.Copy(byte_d, 0, data, curren_pos, byte_d.Length);
                curren_pos += byte_d.Length;
            }
            catch (IndexOutOfRangeException)
            {
                Console.Error.WriteLine($"индекс находится за границами массива id client: {GetIdClient()} type: {GetTypePacket()}");
            }
        }

        public void Write(float f)
        {
            Write(BitConverter.GetBytes(f));
        }


    }
}
