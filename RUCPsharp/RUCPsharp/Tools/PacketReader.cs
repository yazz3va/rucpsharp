﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RUCPsharp.Tools
{
    public partial class Packet : IComparable<Packet>
    {
        /// <summary>
        /// Возвращает текущую позицию индекса и увеличвает позициюю индекса на размер считывания
        /// </summary>
        /// <param name="len"></param>
        /// <returns></returns>
        public int ReadIndex(int len)
        {
            int ret = curren_pos;
            curren_pos += len;
            return ret;
        }


        public int ReadInt()
        {
            int ret = BitConverter.ToInt32(data, curren_pos);
            curren_pos += 4;
            return ret;
        }

        public float ReadFloat()
        {
            float ret = BitConverter.ToSingle(data, curren_pos);
            curren_pos += 4;
            return ret;
        }

        public short ReadShort()
        { 
            short ret = BitConverter.ToInt16(data, curren_pos);
            curren_pos += 2;
            return ret;
        }

        public byte ReadByte()
        {
            byte ret = data[curren_pos];
            curren_pos++;
            return ret;
        }

        public byte[] Read(int len)
        {
            try
            {
                byte[] ret = new byte[len];

                Array.Copy(data, curren_pos, ret, 0, ret.Length);

                curren_pos += len;

                return ret;
            }
            catch (ArgumentOutOfRangeException)
            {
                return new byte[0];
            }
        }
    }
}
