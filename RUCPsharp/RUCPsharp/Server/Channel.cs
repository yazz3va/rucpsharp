﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RUCPsharp.Server
{

    /**
     * Created by yazZ3 on 17.04.2017.
     */
    public static class Channel
    {
        public const byte ACK = 1;
        public const byte Reliable = 2;
        public const byte Unreliable = 3;
        public const byte Connection = 111;
        public const byte Disconnect = 100;

    }
}
