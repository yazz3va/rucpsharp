﻿using MySql.Data.MySqlClient;
using SurvivalServer.DataBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SurvivalServer.DataBaseQuery
{
    class CharactersTable
    {

        public static int GetIdLoginByCharacterId(int id_character, out string name)
        {
            int id_login = 0;
            name = "";
            lock (GDBHandler.Instance())
            {
                using (MySqlDataReader reader = GDBHandler.ExecuteReader("SELECT idlogin, namecharacter FROM characters WHERE idcharacter=" + id_character + ";"))
                {
                    if (reader.Read())
                    {
                        id_login = reader.GetInt32(0);
                        name = reader.GetString(1);
                    }
                }
            }
                return id_login;
        }

        public static IdNameCharacter[] GetCharacters(int id)
        {
            IdNameCharacter[] characters = new IdNameCharacter[7];

            int index = 0;
            lock (GDBHandler.Instance())
            {
                using (MySqlDataReader reader = GDBHandler.ExecuteReader("SELECT idcharacter, namecharacter FROM characters WHERE idlogin=" + id + ";"))
                {
                    while (reader.Read())
                    {
                        if (index >= 7) { Console.WriteLine($"player {id} has more than 7 characters"); continue; }
                        characters[index] = new IdNameCharacter();
                        characters[index].id = reader.GetInt32(0);
                        characters[index].name = reader.GetString(1);
                        index++;
                    }
                }
            }
            return characters;
        }

        /// <summary>
        /// 0 - Создание прошло успешно
        /// 1 -создать не удалось персонаж с таким именем используеться
        /// 2 - Превышенно максимально допустимое количество персонажей
        /// </summary>
        /// <param name="id"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public static int CreateCharacter(int id, string name)
        {
          
            lock (GDBHandler.Instance())
            {
                //Проверка если персонаж с таким именем в таблице персонажей
                using (MySqlDataReader reader = GDBHandler.ExecuteReader("SELECT namecharacter FROM characters WHERE namecharacter='" + name + "';"))
                {
                    if (reader.Read())
                    {
                        return 1;
                    }
                }

                //Подсчет количества персонажей созданных игроком
                int count_characters = 0;
                    using (MySqlDataReader reader = GDBHandler.ExecuteReader("SELECT idlogin FROM characters WHERE idlogin=" + id + ";"))
                    {
                        if (reader.Read())
                        {
                        count_characters++;
                        }
                    }
                if (count_characters > 7) return 2;

                //Если нет
                
                    //Создаем персонажа
                    GDBHandler.ExecuteNonQuery("INSERT INTO characters (idlogin,namecharacter) VALUES(" + id + ",'" + name + "');");

                    int id_character = 0;
                    //Считываем его ид
                    using (MySqlDataReader reader = GDBHandler.ExecuteReader("SELECT idCharacter FROM characters WHERE namecharacter='" + name + "';"))
                    {
                        if (reader.Read())
                        {
                            id_character = reader.GetInt32(0);
                        }
                    }
                    //И создаем новую строку с информацией о персонаже в таблице информации о персонаже
                    GDBHandler.ExecuteNonQuery("INSERT INTO characterinfo (idCharacter,HP) VALUES(" + id_character + "," + 100 + ");");
               
            }
            return 0;
        }
    }

    class IdNameCharacter
    {
        public int id { get; set; }
        public string name { get; set; }
    }
}
