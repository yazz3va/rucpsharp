﻿using MySql.Data.MySqlClient;
using SurvivalServer.DataBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SurvivalServer.DataBaseQuery
{
    class CharacterInfoTable
    {
        public static int GetHP(int id_character)
        {
            int HP = 0;
            lock (GDBHandler.Instance())
            {
                using (MySqlDataReader reader = GDBHandler.ExecuteReader("SELECT HP FROM characterinfo WHERE idCharacter=" + id_character + ";"))
                {
                    if (reader.Read())
                    {
                        HP = reader.GetInt32(0);
                    }
                }
            }
            return HP;
        }

        public static void SetHP(int id_character, int HP)
        {

            lock (GDBHandler.Instance())
            {
                GDBHandler.ExecuteNonQuery("UPDATE characterinfo SET HP=" + HP + " WHERE idCharacter=" + id_character + ";");
            }

        }
    }
}
