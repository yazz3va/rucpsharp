﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RUCPsharp.Server;
using RUCPsharp.Tools;
using RUCPsharp.Sender;
using SurvivalServer.Profiles;
using SurvivalServer.HandlerType;
using SurvivalServer.DataBase;

namespace SurvivalServer
{
    class Program
    {
        class Hand: Handler
        {
            public override HandlerPack getHandler()
            {
                Profile hd = new Profile();
                return hd;
            }
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Survival Game Server ver. 0.02a");
            LDBHandler.Connection();
            RegisteredTypes.RegisterTypes();

            ServerMain server = new ServerMain(3232);

            server.setHandler(new Hand());

            server.start();
        }



}
}
