﻿using RUCPsharp.Sender;
using RUCPsharp.Server;
using RUCPsharp.Tools;
using SurvivalServer.DataBaseQuery;
using SurvivalServer.HandlerType;
using SurvivalServer.Profiles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SurvivalServer.CharactersRoom
{
    class CharactersRoomGet
    {

        public static void Get(Packet pack, PlayerInfo info)
        {

            IdNameCharacter[] characters = CharactersTable.GetCharacters(info.Id);
           
            int length = 0;
            foreach (IdNameCharacter character in characters)
            {
                if (character == null) continue;
                length += 5;
                length += character.name.Length;
            }
            Console.WriteLine(length);
            pack.reuse(Channel.Reliable, length);
            pack.writeType(Types.CharactersList);

            foreach (IdNameCharacter character in characters)
            {
                if (character == null) continue;
                pack.write(character.id);
                pack.write((byte)character.name.Length);
                pack.write(Encoding.ASCII.GetBytes(character.name));
            }

            SenderClass.send(pack);
        }
    }
}
