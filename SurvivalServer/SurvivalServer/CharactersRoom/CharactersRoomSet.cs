﻿using RUCPsharp.Sender;
using RUCPsharp.Server;
using RUCPsharp.Tools;
using SurvivalServer.DataBaseQuery;
using SurvivalServer.HandlerType;
using SurvivalServer.Profiles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SurvivalServer.CharactersRoom
{
    class CharactersRoomSet
    {
        public static void Set(Packet pack, PlayerInfo info)
        {
            int id_character = pack.ReadInt();
            int key = pack.ReadInt();

            if(key != info.Sessionkey) { SendAnswer(pack, 1); return; }//Если ключ не соответствует присланаму
            if (info.Id != CharactersTable.GetIdLoginByCharacterId(id_character, out string name)) { SendAnswer(pack, 1); return; } //Если ид игрока не соответствует ид персонажа

            info.Character.Id = id_character;
            info.Character.name = Encoding.ASCII.GetBytes(name);

            SendAnswer(pack, 0);
        }

        /// 0 - Выбор успешен
        /// 1 -ошибка
        private static void SendAnswer(Packet pack, byte answer)
        {
            pack.reuse(Channel.Reliable, 1);
            pack.writeType(Types.SetCharacter);
            pack.write(answer);
            SenderClass.send(pack);
        }
    }
}
