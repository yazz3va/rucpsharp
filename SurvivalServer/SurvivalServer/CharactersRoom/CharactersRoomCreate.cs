﻿using RUCPsharp.Sender;
using RUCPsharp.Server;
using RUCPsharp.Tools;
using SurvivalServer.DataBaseQuery;
using SurvivalServer.HandlerType;
using SurvivalServer.Profiles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SurvivalServer.CharactersRoom
{
    class CharactersRoomCreate
    {
        public static void Create(Packet pack, PlayerInfo info)
        {
            string name = Encoding.ASCII.GetString(pack.Read(pack.ReadByte()));



            //Если ошибка
            SendAnswer(pack, (byte)CharactersTable.CreateCharacter(info.Id, name));

        }

        /// 0 - Создание прошло успешно
        /// 1 -создать не удалось персонаж с таким именем используеться
        /// 2 - Превышенно максимально допустимое количество персонажей
        private static void SendAnswer(Packet pack, byte answer)
        {
            pack.reuse(Channel.Reliable, 1);
            pack.writeType(Types.CreateCharacterInRoom);
            pack.write(answer);
            SenderClass.send(pack);
        }
    }
}
