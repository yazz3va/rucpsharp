﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SurvivalServer.Profiles
{
    class CharacterInfo
    {
        public byte[] name { get; set; }
        public int Id { get; set; }
        public int Hp { get; set; }
        public int MaxHp { get; set; }
        public float posX { get; set; }
        public float posY { get; set; }


    }
}
