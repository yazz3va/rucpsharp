﻿using RUCPsharp.ClientSocket;
using SurvivalServer.GameWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SurvivalServer.Profiles
{
    class PlayerInfo:ITarget
    {
        public int Id { get; set; }
        public int Sessionkey { get; set; }
        public CharacterInfo Character { get; set; }
        public ClientInfo Client { get; set; }
        public ITarget Target { get; set; }


        public PlayerInfo()
        {

        }
       public PlayerInfo(CharacterInfo pl, ClientInfo cl, int id)
        {
            Character = pl;
            Client = cl;
            this.Id = id;
        }

        void ITarget.Atack(int damag, int enemy_id)
        {
            Character.Hp -= damag;
            CharacterDamage.SendDamageOut(damag, enemy_id, Client);

            if(Character.Hp < 1)
            {
                CharacterDeath.CharacterDeathToSelf(this); //Отправка себе
                CharacterDeath.CharacterDeatToAll(this);//Отправка всем кроме себя
            }
        }
    }
}
