﻿using RUCPsharp.Sender;
using RUCPsharp.Tools;
using SurvivalServer.DataBaseQuery;
using SurvivalServer.HandlerType;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SurvivalServer.Profiles
{
    class Profile : HandlerPack
    {
        private PlayerInfo player_info = null;
        private RegisteredTypes type;

        public Profile()
        {
            player_info = new PlayerInfo();
            player_info.Character = new CharacterInfo();
            type = RegisteredTypes.Instance();
        }

        bool HandlerPack.OpenConnection(Packet pack)
        {
            int id = pack.ReadInt();
            int key = pack.ReadInt();

            if (key == LoginTable.GetSessionKey(id))
            {
                player_info.Id = id;
                player_info.Sessionkey = key;
                player_info.Client = pack.getClient();
                return true;
            }
            
              
                return false;
        }

        void HandlerPack.ReadFromConnection(Packet pack)
        {
            type[pack.getType()](pack, player_info);
        }

        void HandlerPack.CloseConnection()
        {
            if (CharacterList.ContainsKey(player_info.Id))
            {
                RemoveCharacter.SelfRemove(null, player_info);
            }
        }

       
    }
}
