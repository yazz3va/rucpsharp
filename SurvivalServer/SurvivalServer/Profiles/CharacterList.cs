﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SurvivalServer.Profiles
{
    class CharacterList
    {
        public static ConcurrentDictionary<int, PlayerInfo> characters = new ConcurrentDictionary<int, PlayerInfo>();
       // private static CharacterList<T> this_obj = new CharacterList<T>();

        public static bool Put(int id, PlayerInfo info)
        {
          return  characters.TryAdd(id, info);
        }

        public static bool GetCharecter(int id, out PlayerInfo info)
        {
           return characters.TryGetValue(id, out info);
        }

        public static bool ContainsKey(int key)
        {
            return characters.ContainsKey(key);
        }

        public static bool Remove(int id)
        {

            return characters.TryRemove(id, out PlayerInfo character_info);

        }
      
       
    }
}
