﻿using RUCPsharp.ClientSocket;
using RUCPsharp.Sender;
using RUCPsharp.Server;
using RUCPsharp.Tools;
using SurvivalServer.DataBaseQuery;
using SurvivalServer.HandlerType;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SurvivalServer.Profiles
{
    class RemoveCharacter
    { 
        public static void SelfRemove(Packet pack, PlayerInfo info)
        {
            CharacterList.Remove(info.Id);
            //   Console.WriteLine($"удаление персонажа: {info.id}  <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
            SaveCharacter(info);

           

            //Отправка информации о вышедшем игроке всем игрокам
            InfoToAllCharacters(info);

            pack = new Packet(info.Client, Channel.Reliable, 1);
            pack.writeType(Types.selfCharacterRemove);

            pack.write((byte)1);


            SenderClass.send(pack);

        }

        private static void SaveCharacter(PlayerInfo info)
        {
            CharacterInfoTable.SetHP(info.Character.Id, info.Character.Hp);
        }

        //Отправка пакета с информацией о одном персонаже всем персонажем
        private static void InfoToAllCharacters(PlayerInfo info)
        {


         //   Console.WriteLine($"В массиве на отправку всем игрокам {CharacterList.characters.Count} ");
            foreach (KeyValuePair<int, PlayerInfo> entry in CharacterList.characters)
            {
                Packet pack = new Packet(entry.Value.Client, Channel.Reliable, 4);
                pack.writeType(Types.CharacterRemove);
                pack.write(info.Id);
                SenderClass.send(pack);
            }
        }
    }
}
