﻿using RUCPsharp.ClientSocket;
using RUCPsharp.Sender;
using RUCPsharp.Server;
using RUCPsharp.Tools;
using SurvivalServer.DataBaseQuery;
using SurvivalServer.HandlerType;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace SurvivalServer.Profiles
{
    class CreateCharacter
    {
        public static void SelfCreate(Packet pack, PlayerInfo info)
        {
            LoadingCharacter(info);

            pack.reuse(Channel.Reliable, info.Character.name.Length + 13);
            pack.writeType(Types.selfCharacterCreate);
            pack.write((byte)info.Character.name.Length);
            pack.write(info.Character.name);
            pack.write(info.Character.Hp);
            pack.write(info.Character.posX);
            pack.write(info.Character.posY);

            SenderClass.send(pack);
           
            //Отправка информации о зашедшем игроке всем игрокам
            InfoToAllCharacters(info);
            //Отправка информации о всех игроках в игре зашедшему игроку
            AllInfoToCharater(pack.getClient());
            CharacterList.Put(info.Id, info);
        }

        private static void LoadingCharacter(PlayerInfo info)
        {

            info.Character.Hp = CharacterInfoTable.GetHP(info.Character.Id);
            info.Character.MaxHp = 100;
            info.Character.posX = 0.0f;
            info.Character.posY = 0.0f;
        }

        //Отправка пакета с информацией о одном персонаже всем персонажем
        private static void InfoToAllCharacters(PlayerInfo info)
        {
    

          //  Console.WriteLine($"В массиве на отправку всем игрокам {CharacterList.characters.Count} ");
            foreach (KeyValuePair<int, PlayerInfo> entry in CharacterList.characters)
            {
                Packet pack = new Packet(entry.Value.Client, Channel.Reliable, info.Character.name.Length + 13);
                pack.writeType(Types.CharacterCreate);
                pack.write(info.Id);
                pack.write((byte)info.Character.name.Length);
                pack.write(info.Character.name);
                pack.write(info.Character.posX);
                pack.write(info.Character.posY);
                SenderClass.send(pack);
            }
        }
        private static void AllInfoToCharater(ClientInfo info)
        {
            
           // Console.WriteLine($"В массиве на отправку игроку игроков {CharacterList.characters.Count} ");
            foreach (KeyValuePair<int, PlayerInfo> entry in CharacterList.characters)
            {
                Packet pack = new Packet(info, Channel.Reliable, entry.Value.Character.name.Length + 13);
                pack.writeType(Types.CharacterCreate);
                pack.write(entry.Value.Id);
                pack.write((byte)entry.Value.Character.name.Length);
                pack.write(entry.Value.Character.name);
                pack.write(entry.Value.Character.posX);
                pack.write(entry.Value.Character.posY);
                SenderClass.send(pack);
            }
        }
    }



}
