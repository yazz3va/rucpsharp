﻿using RUCPsharp.Sender;
using RUCPsharp.Server;
using RUCPsharp.Tools;
using SurvivalServer.HandlerType;
using SurvivalServer.Profiles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SurvivalServer.Physics
{
    class PlayerMove
    {

        public static void Move(Packet pack, PlayerInfo info)
        {
            info.Character.posX = BitConverter.ToSingle(pack.getData(), pack.ReadIndex(4));
            info.Character.posY = BitConverter.ToSingle(pack.getData(), pack.ReadIndex(4));
            ToAllCharactes(info);
        }

        private static void ToAllCharactes(PlayerInfo info)
        {


            foreach(KeyValuePair<int, PlayerInfo> entry in CharacterList.characters)
            {
                if (entry.Value.Id == info.Id) continue;

                Packet pack = new Packet(entry.Value.Client, Channel.Unreliable, 12);
                pack.writeType(Types.CharacterMove);
                pack.write(info.Id);
                pack.write(info.Character.posX);
                pack.write(info.Character.posY);
              
                
                SenderClass.send(pack);
            }
        }
    }
}
