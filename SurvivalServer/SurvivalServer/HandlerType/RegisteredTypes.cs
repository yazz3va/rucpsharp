﻿using RUCPsharp.Tools;
using SurvivalServer.CharactersRoom;
using SurvivalServer.GameWorld;
using SurvivalServer.Physics;
using SurvivalServer.Profiles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SurvivalServer.HandlerType
{
    delegate void ChannelRead(Packet pack, PlayerInfo info);
   

     class RegisteredTypes
    {
      private static ChannelRead[] read = null;

        private static RegisteredTypes reg = new RegisteredTypes();

        public static RegisteredTypes Instance()
        {
            return reg;
        }

        public ChannelRead this[int index]
        {
            get
            {
                if(index < 0 || index >= read.Length) return read[0];
                if (read[index] != null) return read[index];
                return read[0];
            }
        }

        public static void RegisterTypes()
        {
            if (read != null) return;
            read = new ChannelRead[256];

            read[0] = Unknown.Type;
            read[Types.ConnectionGameWorld] = WorldEntrance.Entrance;
            read[Types.PlayerMove] = PlayerMove.Move;
            read[Types.selfCharacterRemove] = RemoveCharacter.SelfRemove;
            read[Types.CreateCharacterInRoom] = CharactersRoomCreate.Create;
            read[Types.CharactersList] = CharactersRoomGet.Get;
            read[Types.SetCharacter] = CharactersRoomSet.Set;
            read[Types.Target] = Target.SetTarget;
            read[Types.AtackSkill] = SkillAtack.Use;
            read[Types.Revival] = CharacterRevival.Revival;
        }
    }
}
