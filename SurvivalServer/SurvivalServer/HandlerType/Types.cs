﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SurvivalServer.HandlerType
{
    public static class Types
    {
        public const short ConnectionGameWorld = 1;// С->S отпровляет запрос серверу на подключение к игровому миру
        public const short selfCharacterCreate = 2; //s->c Информация для создания своего персонажа
        public const short CharacterCreate = 3; //S-C данные для создания обьекта персонажа на клиенте 
        public const short PlayerMove = 4; //C->S->C Отправка на сервер нового положение игрока и отпрака обратно подтвержденного положение
        public const short CharacterMove = 5; //S->C Отправка на клиент положения персонажей
        public const short CharacterRemove = 6; //S->C Удаляет обьект персонажа на клиенте
        public const short selfCharacterRemove = 7; // S->C Выход из карты
        public const short Login = 8;  //C->S отпровляет логин и пароль на сервер
        public const short Registration = 9; //C->S отпровляет логин и пароль  для регистрации акаунта
        public const short LoginInfo = 10; //S->C  Отпровляет код ошибки или успешного выполнение запроса
        public const short LoginOk = 11; //S->C Логин успешен
        public const short ServersList = 12; //C->S запрос на получение списка серверов S->C Список серверов
        public const short CreateCharacterInRoom = 13; //C->S Запрос на создание персонажа в комнате персонажей S->C 1 - персонаж создан успешно 2- такое имя не существует
        public const short CharactersList = 14; //C->S Запрос на получение списка персонажей S->C Список персонажей
        public const short SetCharacter = 15; //С->S Запрос на выбор персонажа. S->C Подтверждение
        public const short Target = 16; //C->S Запрос на выбор цели по ид. S->C ответ можно ли взять цель в таргет
        public const short AtackSkill = 17;//C->S Использвание атакуещего уменя на таргет
        public const short DamageIn = 18;
        public const short DamageOut = 19;
        public const short CharacterDeath = 20;
        public const short PlayerDeath = 21;
        public const short CharacterRevival = 22;
        public const short PlayerRevival = 23;
        public const short Revival = 24;
    }
}
