﻿using RUCPsharp.Sender;
using RUCPsharp.Server;
using RUCPsharp.Tools;
using SurvivalServer.HandlerType;
using SurvivalServer.Profiles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SurvivalServer.GameWorld
{
    class Target
    {
        public static void SetTarget(Packet pack, PlayerInfo info)
        {
            int id_target = pack.ReadInt();
            if(id_target == -1)//Команда на закрытие таргета
            {
                info.Target = null;
                OnTarget(pack);
                return;
            }
            CharacterList.GetCharecter(id_target, out PlayerInfo target_info);
            if (target_info != null)
            {
                info.Target = target_info;
                OnTarget(pack);
                return;
            }
           
        }

        //Если удалось установить таргет отправка пакета на клиент с ид цели
        private static void OnTarget(Packet pack)
        {
            SenderClass.send(pack);
        }
        
    }
}
