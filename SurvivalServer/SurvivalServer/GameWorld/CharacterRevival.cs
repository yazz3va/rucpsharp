﻿using RUCPsharp.Sender;
using RUCPsharp.Server;
using RUCPsharp.Tools;
using SurvivalServer.HandlerType;
using SurvivalServer.Profiles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SurvivalServer.GameWorld
{
    class CharacterRevival
    {
        public static void Revival(Packet pack, PlayerInfo info)
        {
            info.Character.Hp = info.Character.MaxHp;
            CharacterRevivalToSelf(info);
            CharacterRevivalToAll(info);
        }

        public static void CharacterRevivalToAll(PlayerInfo player)
        {
            foreach (KeyValuePair<int, PlayerInfo> entry in CharacterList.characters)
            {
                PlayerInfo character = entry.Value;
                if (player.Id == character.Id) continue; //Отправка всем кроме себя

                Packet pack = new Packet(character.Client, Channel.Reliable, 16);
                pack.writeType(Types.CharacterRevival);
                pack.write(player.Id);
                pack.write(player.Character.Hp);
                pack.write(player.Character.posX);
                pack.write(player.Character.posY);
                SenderClass.send(pack);
            }
        }

        public static void CharacterRevivalToSelf(PlayerInfo player)
        {
            Packet pack = new Packet(player.Client, Channel.Reliable, 12);
            pack.writeType(Types.PlayerRevival);
            pack.write(player.Character.Hp);
            pack.write(player.Character.posX);
            pack.write(player.Character.posY);
            SenderClass.send(pack);
        }
    }
}
