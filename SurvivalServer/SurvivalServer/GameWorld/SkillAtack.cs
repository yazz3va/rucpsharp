﻿using RUCPsharp.Tools;
using SurvivalServer.Profiles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SurvivalServer.GameWorld
{
    class SkillAtack
    {
        public static void Use(Packet pack, PlayerInfo info)
        {
            if(info.Target == null) { Console.WriteLine("target not found"); return; }

            //TODO: distance to target

            info.Target.Atack(30, info.Id);

            CharacterDamage.SendDamageIn(30,  info.Client);
        }
    }
}
