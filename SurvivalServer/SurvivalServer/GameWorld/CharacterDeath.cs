﻿using RUCPsharp.Sender;
using RUCPsharp.Server;
using RUCPsharp.Tools;
using SurvivalServer.HandlerType;
using SurvivalServer.Profiles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SurvivalServer.GameWorld
{
    class CharacterDeath
    {
        public static void CharacterDeatToAll(PlayerInfo player)
        {
            foreach (KeyValuePair<int, PlayerInfo> entry in CharacterList.characters)
            {
                PlayerInfo character = entry.Value;
                if (player.Id == character.Id) continue; //Отправка всем кроме себя

                Packet pack = new Packet(character.Client, Channel.Reliable, 12);
                pack.writeType(Types.CharacterDeath);
                pack.write(player.Id);
                pack.write(player.Character.posX);
                pack.write(player.Character.posY);
                SenderClass.send(pack);
            }
        }

        public static void CharacterDeathToSelf(PlayerInfo player)
        {
            Packet pack = new Packet(player.Client, Channel.Reliable, 8);
            pack.writeType(Types.PlayerDeath);
            pack.write(player.Character.posX);
            pack.write(player.Character.posY);
            SenderClass.send(pack);
        }
    }
}
