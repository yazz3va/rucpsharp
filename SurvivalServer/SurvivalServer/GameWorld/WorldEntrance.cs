﻿using RUCPsharp.Tools;
using SurvivalServer.Profiles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SurvivalServer.GameWorld
{
    class WorldEntrance
    {
        public static void Entrance(Packet pack, PlayerInfo info)
        {
            CreateCharacter.SelfCreate(pack, info);
            if(info.Character.Hp < 1)
            {
                CharacterDeath.CharacterDeathToSelf(info); //Отправка себе
                CharacterDeath.CharacterDeatToAll(info);//Отправка всем кроме себя
            }
        }
    }
}
