﻿using RUCPsharp.ClientSocket;
using RUCPsharp.Sender;
using RUCPsharp.Server;
using RUCPsharp.Tools;
using SurvivalServer.HandlerType;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SurvivalServer.GameWorld
{
    class CharacterDamage
    {
        /// <summary>
        /// Нанесенный по противнику урон
        /// </summary>
        /// <param name="damag"></param>
        /// <param name="client"></param>
        public static void SendDamageIn(int damag,  ClientInfo client)
        {
            Packet packet = new Packet(client, Channel.Reliable, 4);
            packet.writeType(Types.DamageIn);

            packet.write(damag);
            SenderClass.send(packet);
        }


        /// <summary>
        /// Полученный урон
        /// </summary>
        /// <param name="damag"></param>
        /// <param name="client"></param>
        public static void SendDamageOut(int damag, int id_enemy, ClientInfo client)
        {
            Packet packet = new Packet(client, Channel.Reliable, 8);
            packet.writeType(Types.DamageOut);
            packet.write(id_enemy);
            packet.write(damag);
            SenderClass.send(packet);
        }

    }
}
